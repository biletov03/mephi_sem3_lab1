#include <string>
#include "catch.hpp"
#include "../../../mephi_sem2_lab2/array_sequence.hpp"
#include "../../Sorts.hpp"
using namespace std;


int compare(const int& i, const int& j) {
    return i - j;
}

TEST_CASE("Sequence test", "[Sequence]") {
   SECTION("Append") {
      Sequence<int> *arr = new ArraySequence<int>();
      for (int i = 0; i < 50; ++i) {
          arr->Append(i);
      }

      REQUIRE(arr->GetLength() == 50);

      for (int i = 0; i < 50; ++i) {
          REQUIRE(arr->Get(i) == i);
      }
   }

    SECTION("Prepend") {
       Sequence<int> * arr = new ArraySequence<int>();
       for (int i = 0; i < 50; ++i) {
           arr->Prepend(i);
       }

       REQUIRE(arr->GetLength() == 50);

       for (int i = 0; i < 50; ++i) {
           REQUIRE(arr->Get(49 - i) == i);
       }
   }

    SECTION("Copy") {
        Sequence<int> *arr = new ArraySequence<int>();
        for (int i = 0; i < 50; ++i) {
            arr->Append(i);
        }
        auto arr_copy = arr->Copy();

        REQUIRE(arr_copy->GetLength() == 50);
        for (int i = 0; i < 50; ++i) {
            REQUIRE(arr_copy->Get(i) == i);
        }
   }
}

TEST_CASE("Isort", "SORT") {
    SECTION("QUICK") {
        auto arr = new ArraySequence<int>();
        auto q = QuickSort<int>();
        for (int i = 0; i < 50; ++i) {
            arr->Append((i + 22) % 50);
        }
        auto res = q.Sort(arr, compare);

        for (int i = 0; i < 50; ++i) {
            REQUIRE(res->Get(i) == i);
        }
    }

    SECTION("Shell") {
        auto arr = new ArraySequence<int>();
        auto s = ShellSort<int>();
        for (int i = 0; i < 50; ++i) {
            arr->Append((i + 22) % 50);
        }
        auto res = s.Sort(arr, compare);

        for (int i = 0; i < 50; ++i) {
            REQUIRE(res->Get(i) == i);
        }
    }

    SECTION("Heap") {
        auto arr = new ArraySequence<int>();
        auto h = HeapSort<int>();
        for (int i = 0; i < 50; ++i) {
            arr->Append((i + 22) % 50);
        }
        auto res = h.Sort(arr, compare);

        for (int i = 0; i < 50; ++i) {
            REQUIRE(res->Get(i) == i);
        }
    }

}