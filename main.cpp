#include <iostream>
#include "../mephi_sem2_lab2/array_sequence.hpp"
#include "Sorts.hpp"
#include <chrono>
#include <cmath>
#include <fstream>

namespace menu {
    bool quick_sort = true;
    bool heap_sort = true;
    bool shell_sort = true;
    string output = "output";
    int poww = 5;
    int step = 4;
    int sort_array = 1;


    void type_of_sort() {
        int type = 0;
        do {
            cout << "\x1B[2J\x1B[H";
            printf("Select sort:\n");
            cout << "1) Quicksort: " << quick_sort << endl;
            cout << "2) Heapsort: " << heap_sort << endl;
            cout << "3) Shellsort: " << shell_sort << endl;
            cout << "4) Exit" << endl;
            cin >> type;
            switch(type) {
                case(1):
                    if (quick_sort) {
                        quick_sort = false;
                    } else {
                        quick_sort = true;
                    }
                    break;
                case(2):
                    if(heap_sort) {
                        heap_sort = false;
                    } else {
                        heap_sort = true;
                    }
                    break;
                case(3):
                    if (shell_sort) {
                        shell_sort = false;
                    } else {
                        shell_sort = true;
                    }
                    break;
                case(4):
                    break;
                default:
                    cout << "\x1B[2J\x1B[H";
                    cout << "Invalid input" << endl;
                    break;
            }
        } while(type != 4);
    }

    void number_of_sorting() {
        int type;
        do {
            cout << "\x1B[2J\x1B[H";
            cout << "Number of element: 10 ^ " << poww << endl;
            cout << "Step: 10 ^ " << step << endl;
            cout << "1) Number -" << endl;
            cout << "2) Number +" << endl;
            cout << "3) Step -" << endl;
            cout << "4) Step +" << endl;
            cout << "5) Exit" << endl;
            cin >> type;
            switch(type) {
                case(1):
                    if (poww > step) {
                        poww--;
                    } else {
                        cout << "\x1B[2J\x1B[H";
                        cout << "We have min number" << endl;
                    }
                    break;
                case(2):
                    poww++;
                    break;
                case(3):
                    if (step > 0) {
                        step--;
                    } else {
                        cout << "\x1B[2J\x1B[H";
                        cout << "We have min step" << endl;
                    }
                    break;
                case(4):
                    if (step < poww) {
                        step++;
                    } else {
                        cout << "\x1B[2J\x1B[H";
                        cout << "We can't make this" << endl;
                    }
                    break;
                default:
                    cout << "\x1B[2J\x1B[H";
                    cout << "Invalid input" << endl;
                    break;
            }
        } while (type != 5);
    }

    void sort_of_massive() {
        int type;
        cout << "\x1B[2J\x1B[H";
        cout << "1) Random" << endl;
        cout << "2) Sort array" << endl;
        cout << "3) Rev sort array" << endl;
        cin >> type;
        if (type >= 1 && type <= 3) {
            sort_array = type;
        } else {
            cout << "\x1B[2J\x1B[H";
            cout << "Invalid input" << endl;
            sort_array = 1;
        }
    }

    template<typename Type>
    void make_random_massive(Sequence<Type> * arr, int len) {
        int start = arr->GetLength();
        int del = (int)pow(10, poww);
        for (int i = start; i < len; ++i) {
            arr->Append((int)(rand() % del));
        }
    }

    void make_notrandom_massive(Sequence<int> * arr, int len) {
        int start = arr->GetLength();
        if (sort_array == 2) {
            for (int i = start; i < len; ++i) {
                arr->Append(i + 1);
            }
        } else {
            for (int i = start; i < len; ++i) {
                arr->Prepend(i + 1);
            }
        }
    }
}

template<typename Type>
void printarr(Sequence<Type> * arr) {
    int n = arr->GetLength();
    for (int i = 0; i < n; ++i) {
        cout << arr->Get(i) << " ";
    }
    cout << endl;
}

int compare(const int& i, const int& j) {
    return i - j;
}

int main() {
    int type;
    int len = 0;
    auto arr = new ArraySequence<int>();
    auto quick = QuickSort<int>();
    auto quick_time = new ArraySequence<double>();
    auto shell = ShellSort<int>();
    auto shell_time = new ArraySequence<double>();
    auto heap = HeapSort<int>();
    auto heap_time = new ArraySequence<double>();
    do {
        cout << "\x1B[2J\x1B[H";
        cout << "0) Tests" << endl;
        cout << "1) Type of sorts" << endl;
        cout << "2) Number of elements" << endl;
        cout << "3) Sort or not" << endl;
        cout << "4) Sorting" << endl;
        cout << "5) Exit" << endl;
        cin >> type;
        cout << "\x1B[2J\x1B[H";
        switch (type) {
            case(0):
                system(((string)"g++ -std=c++11 ../CodingSandboxAllLingos/TestingWithCatch/*.cpp -o res.exe").c_str());
                system(((string)"./res.exe").c_str());
                system(((string)"rm res.exe").c_str());
                return 0;
            case(1):
                menu::type_of_sort();
                break;
            case(2):
                menu::number_of_sorting();
                break;
            case(3):
                menu::sort_of_massive();
                break;
            case(4):
                do {
                    len += (int)pow(10, menu::step);
                    if (menu::sort_array == 1) {
                        menu::make_random_massive(arr, len);
                    } else {
                        menu::make_notrandom_massive(arr, len);
                    }
                    if(menu::quick_sort == 1) {
                        auto start = std::chrono::high_resolution_clock::now();
                        quick.Sort(arr, compare);
                        auto end = std::chrono::high_resolution_clock::now();
                        std::chrono::duration<double, std::milli> time = end - start;
                        quick_time->Append(time.count());
                    }
                    if(menu::shell_sort == 1) {
                        auto start = std::chrono::high_resolution_clock::now();
                        shell.Sort(arr, compare);
                        auto end = std::chrono::high_resolution_clock::now();
                        std::chrono::duration<double, std::milli> time = end - start;
                        shell_time->Append(time.count());
                    }
                    if(menu::heap_sort == 1) {
                        auto start = std::chrono::high_resolution_clock::now();
                        heap.Sort(arr, compare);
                        auto end = std::chrono::high_resolution_clock::now();
                        std::chrono::duration<double, std::milli> time = end - start;
                        heap_time->Append(time.count());
                    }

                } while (len != pow(10, menu::poww));
                break;
            case(5):
                menu::heap_sort = false;
                menu::quick_sort = false;
                menu::shell_sort  = false;
                break;
            default:
                cout << "\x1B[2J\x1B[H";
                cout << "Invalid input" << endl;
                break;
        }
    } while (type != 5 && type != 4);
    if (menu::shell_sort || menu::quick_sort || menu::heap_sort) {
        fstream file;
        file.open(menu::output + ".csv", ios::out);
        if (menu::quick_sort) {
            file << "quick,";
            for (int i = 0; i < quick_time->GetLength(); ++i) {
                file << quick_time->Get(i) << ',';
            }
            file << '\n';
        }
        if (menu::shell_sort) {
            file << "shell,";
            for (int i = 0; i < shell_time->GetLength(); ++i) {
                file << shell_time->Get(i) << ',';
            }
            file << '\n';
        }
        if (menu::heap_sort) {
            file << "heap,";
            for (int i = 0; i < heap_time->GetLength(); ++i) {
                file << heap_time->Get(i) << ',';
            }
            file << '\n';
        }
        file << "size,";
        int count = pow(10, menu::poww - menu::step);
        for (int i = 1; i <= count; ++i) {
            file << i * pow(10, menu::step) << ',';
        }
        file << '\n';
        file.close();
        system(("python3 ../graf/graf.py ../cmake-build-debug/" + menu::output).c_str());
    }
    return 0;
}
