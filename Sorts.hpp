#ifndef MAIN_CPP_SORTS_HPP
#define MAIN_CPP_SORTS_HPP
#include "../mephi_sem2_lab2/array_sequence.hpp"

template<typename Type>
class Isort {
public:
    virtual Sequence<Type> * Sort(Sequence<Type> * arr, int (*cmp)(const Type& i, const Type& j)) = 0;
};

template<typename Type>
class QuickSort : public Isort<Type> {
private:
    void Qsorter(Sequence<Type> * seq, int low, int high, int (*cmp)(const Type& i,const Type& j)) {
        int i = low;
        int j = high;
        Type buf = seq->Get((i + j) / 2);

        while (i <= j) {
            while (cmp(seq->Get(i), buf) < 0) ++i;

            while (cmp(seq->Get(j), buf) > 0) --j;
            if (i <= j) {
                seq->Swap(i, j);
                ++i;
                --j;
            }
        }
        if (j > low) Qsorter(seq, low, j, cmp);
        if (i < high) Qsorter(seq, i, high, cmp);
    }

public:
    QuickSort() = default;

    Sequence<Type> * Sort(Sequence<Type> * seq, int (*cmp)(const Type& i, const Type& j)) override {
        auto res = seq->Copy();
        Qsorter(res, 0, seq->GetLength() - 1, cmp);
        return res;
    }
};

template<typename Type>
class HeapSort : public Isort<Type> {
private:
    void Heapify(Sequence<Type> * seq, int length, int i, int (*cmp)(const Type& i, const Type& j)) {
        int largest = i;

        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < length && cmp(seq->Get(left), seq->Get(largest)) > 0) {
            largest = left;
        }

        if (right < length && cmp(seq->Get(right), seq->Get(largest)) > 0) {
            largest = right;
        }

        if (largest != i) {
            seq->Swap(i, largest);
            Heapify(seq, length, largest, cmp);
        }
    }

public:
    HeapSort() = default;

    Sequence<Type>* Sort(Sequence<Type> * seq, int (*cmp)(const Type& i, const Type& j)) override {
        auto res = seq->Copy();

        int length = res->GetLength();
        for (int i = (length / 2) - 1; i >= 0; i--) {
            Heapify(res, length, i, cmp);
        }

        for (int i = length - 1; i >= 0; i--) {
            res->Swap(0, i);

            Heapify(res, i, 0, cmp);
        }
        return res;
    }
};

template<typename Type>
class ShellSort : public Isort<Type> {
public:
    ShellSort() = default;

    Sequence<Type>* Sort(Sequence<Type> * seq, int (*cmp)(const Type& i, const Type& j)) override {
        auto res = seq->Copy();
        int length = res->GetLength();
        for (int gap = length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < length; ++i) {
                Type temp = res->Get(i);
                int j;
                for (j = i; j >= gap && cmp(res->Get(j - gap), temp) > 0; j -= gap) {
                    res->Set(res->Get(j - gap), j);
                }
                res->Set(temp, j);
            }
        }
        return res;
    }
};

#endif //MAIN_CPP_SORTS_HPP
